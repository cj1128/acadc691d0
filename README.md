# Airwallex - Frontend Code Challenge

## Setup

This project uses **pnpm (npm install -g pnpm)** to manage dependencies.

```bash
$ git clone ...
# install depenencies
$ pnpm install
# start dev server
$ pnpm dev
```

- `pnpm build`: build for production
- `pnpm test`: test the project
