module.exports = {
  transform: {
    '^.+\\.(js|ts)x?$': 'babel-jest',
  },
}
