import React, { FC, useState } from 'react'
import Button from '@material-ui/core/Button'
import InviteDialog from './InviteDialog'
import SuccessDialog from './SuccessDialog'
import { SnackbarProvider } from 'notistack'

const Header: FC<{}> = () => {
  return (
    <div className="py-4 border-b border-gray-200 shadow">
      <div className="pl-8 font-bold text-blue-600">BROCCOLI & CO.</div>
    </div>
  )
}

const Footer: FC<{}> = () => {
  return (
    <div className="text-center py-2 border-t border-gray-200 text-gray-400 text-sm">
      <p>Made with &hearts; in Melbourne.</p>
      <p>&copy; 2021 Broccoli & Co. All rights reserved.</p>
    </div>
  )
}

const Main: FC<{}> = () => {
  const [showInvite, setShowInvite] = useState(false)
  const [showSuccess, setShowSuccess] = useState(false)

  const onRequest = () => {
    setShowInvite(true)
  }

  const onSuccess = () => {
    setShowInvite(false)
    setShowSuccess(true)
  }

  return (
    <div className="flex-1">
      <section className="text-center flex flex-col items-center h-full">
        <h2 className="mt-auto text-7xl text-gray-700">
          A better way
          <br />
          to enjoy every day.
        </h2>

        <p className="my-8">Be the first to know when we launch.</p>

        <div className="mb-auto">
          <Button
            data-testid="btn"
            color="primary"
            variant="contained"
            onClick={onRequest}
          >
            Request an invite
          </Button>
        </div>

        <InviteDialog
          key={showInvite.toString()} // clear dialog state
          onSuccess={onSuccess}
          open={showInvite}
          onClose={() => setShowInvite(false)}
        />

        <SuccessDialog
          onClose={() => setShowSuccess(false)}
          open={showSuccess}
        />
      </section>
    </div>
  )
}

const App: FC<{}> = () => {
  return (
    <SnackbarProvider>
      <main className="h-screen flex flex-col">
        <Header />

        <Main />

        <Footer />
      </main>
    </SnackbarProvider>
  )
}

export default App
