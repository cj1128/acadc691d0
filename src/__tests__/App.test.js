/**
 * @jest-environment jsdom
 */

import React from 'react'
import {
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react'
import '@testing-library/jest-dom'
import userEvent from '@testing-library/user-event'
import App from '../App.tsx'

describe('Request an invite', () => {
  test('show dialog when click the invite button', async () => {
    render(<App />)

    await userEvent.click(screen.getByTestId('btn'))
    expect(screen.getByTestId('invite-dialog')).toBeInTheDocument()
  })

  test('name length must >= 3', async () => {
    render(<App />)
    await userEvent.click(screen.getByTestId('btn'))

    await userEvent.type(screen.getByTestId('email-input'), 'a@a.com')
    await userEvent.type(screen.getByTestId('confirm-email-input'), 'a@a.com')

    await userEvent.type(screen.getByTestId('name-input'), 'ab')

    await userEvent.click(screen.getByTestId('send-btn'))

    expect(
      screen.getByText('Need to be at least 3 characters')
    ).toBeInTheDocument()
  })

  test('email must be valid', async () => {
    render(<App />)
    await userEvent.click(screen.getByTestId('btn'))

    await userEvent.type(screen.getByTestId('name-input'), 'abcd')
    await userEvent.type(screen.getByTestId('email-input'), 'abc')
    await userEvent.type(screen.getByTestId('confirm-email-input'), 'abc')

    await userEvent.click(screen.getByTestId('send-btn'))

    expect(
      screen.getByText('Must be a valid email address')
    ).toBeInTheDocument()
  })

  test('confirm email must be equal to email', async () => {
    render(<App />)
    await userEvent.click(screen.getByTestId('btn'))

    await userEvent.type(screen.getByTestId('name-input'), 'abcd')
    await userEvent.type(screen.getByTestId('email-input'), 'abc@abc.com')
    await userEvent.type(
      screen.getByTestId('confirm-email-input'),
      'def@def.com'
    )

    await userEvent.click(screen.getByTestId('send-btn'))

    expect(screen.getByText('Must match email')).toBeInTheDocument()
  })

  test('400 bad request', async () => {
    render(<App />)

    await userEvent.click(screen.getByTestId('btn'))

    await userEvent.type(screen.getByTestId('name-input'), 'foo')
    await userEvent.type(
      screen.getByTestId('email-input'),
      'usedemail@airwallex.com'
    )
    await userEvent.type(
      screen.getByTestId('confirm-email-input'),
      'usedemail@airwallex.com'
    )

    await userEvent.click(screen.getByTestId('send-btn'))

    await waitForElementToBeRemoved(
      () => screen.getByText('Sending, please wait...'),
      {
        timeout: 50000,
      }
    )

    expect(screen.getByTestId('errmsg')).toBeInTheDocument()
  })

  test('200 show success', async () => {
    render(<App />)

    await userEvent.click(screen.getByTestId('btn'))

    await userEvent.type(screen.getByTestId('name-input'), 'foo')
    await userEvent.type(screen.getByTestId('email-input'), 'abc@abc.com')
    await userEvent.type(
      screen.getByTestId('confirm-email-input'),
      'abc@abc.com'
    )

    await userEvent.click(screen.getByTestId('send-btn'))

    await waitForElementToBeRemoved(
      () => screen.getByText('Sending, please wait...'),
      {
        timeout: 50000,
      }
    )

    expect(screen.getByTestId('success-dialog')).toBeInTheDocument()
  })
})
