import React, { FC, useState } from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import Button from '@material-ui/core/Button'
import DialogTitle from '@material-ui/core/DialogTitle'

const SuccessDialog: FC<{
  open: boolean
  onClose: () => void
}> = ({ open, onClose }) => {
  return (
    <div data-testid="success-dialog">
      <Dialog open={open}>
        <DialogTitle>All done!</DialogTitle>

        <DialogContent>
          You will be one of the first to experience Broccoli & Co. when we
          launch.
        </DialogContent>

        <DialogActions>
          <Button color="primary" variant="contained" onClick={onClose}>
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default SuccessDialog
