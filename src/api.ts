import axios from 'axios'

// 200 or 400
// resolve with error msg
export const sendRequest = (name: string, email: string): Promise<string> => {
  return axios
    .post(
      'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth',
      {
        name,
        email,
      },
      {
        validateStatus: s => s === 200 || s === 400,
      }
    )
    .then(res => {
      if (res.status === 200) return ''

      return res.data.errorMessage
    })
}
