import React, { FC, useState } from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import Button from '@material-ui/core/Button'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import Alert from '@material-ui/lab/Alert'
import { sendRequest } from './api'
import { useSnackbar } from 'notistack'

// NOTE(cj): Current length checking works but is not perfect.
// It *does not* consider unicode graphemes.
// e.g. 👩‍🍳 will be considered 3 characters.
// fix this if we need.
const strLength = (str: string) => [...str].length

// copied from https://github.com/arasatasaygin/is.js/blob/master/is.js
const isValidEmail = (email: string) => {
  return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(
    email
  )
}

const InviteDialog: FC<{
  open: boolean
  onSuccess: () => void
  onClose?: () => void
}> = ({ open, onSuccess, onClose }) => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [confirmEmail, setConfirmEmail] = useState('')

  // form input error
  const [err, setErr] = useState({
    name: false,
    email: false,
    confirmEmail: false,
  })

  // server error
  const [errMsg, setErrMsg] = useState('')

  const [isLoading, setIsLoading] = useState(false)
  const { enqueueSnackbar } = useSnackbar()
  const onClick = () => {
    setErrMsg('') // clear server error msg

    const e = { ...err }
    e.name = strLength(name) < 3
    e.email = !isValidEmail(email)
    e.confirmEmail = email !== confirmEmail

    setErr(e)

    // has error
    if (e.name || e.email || e.confirmEmail) {
      return
    }

    setIsLoading(true)
    sendRequest(name, email)
      .then(msg => {
        if (msg === '') return onSuccess()

        // business error
        setErrMsg(msg)
        setIsLoading(false)
      })
      .catch(err => {
        setIsLoading(false)
        // other error
        enqueueSnackbar(err.message, { variant: 'error' })
      })
  }

  const anyFieldEmpty = name === '' || email === '' || confirmEmail === ''

  return (
    <div>
      <Dialog open={open} onClose={onClose} data-testid="invite-dialog">
        <DialogTitle>Request an invite</DialogTitle>

        <div>
          {errMsg !== '' && (
            <Alert data-testid="errmsg" severity="error">
              {errMsg}
            </Alert>
          )}
        </div>

        <DialogContent>
          <TextField
            inputProps={{
              'data-testid': 'name-input',
            }}
            required
            error={err.name}
            helperText="Need to be at least 3 characters"
            value={name}
            onChange={evt => setName(evt.target.value)}
            autoFocus
            margin="dense"
            label="Full name"
            fullWidth
          />

          <TextField
            required
            inputProps={{
              'data-testid': 'email-input',
            }}
            error={err.email}
            value={email}
            onChange={evt => setEmail(evt.target.value)}
            helperText="Must be a valid email address"
            margin="dense"
            label="Email"
            type="email"
            fullWidth
          />

          <TextField
            inputProps={{
              'data-testid': 'confirm-email-input',
            }}
            required
            error={err.confirmEmail}
            value={confirmEmail}
            onChange={evt => setConfirmEmail(evt.target.value)}
            helperText="Must match email"
            margin="dense"
            label="Confirm Email"
            type="email"
            fullWidth
          />
        </DialogContent>

        <DialogActions>
          <Button
            disabled={isLoading || anyFieldEmpty}
            variant="contained"
            color="primary"
            onClick={onClick}
            data-testid="send-btn"
          >
            {isLoading ? 'Sending, please wait...' : 'Send'}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default InviteDialog
